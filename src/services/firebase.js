import firebase from 'firebase/app';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyBqtlU2uIjAAS6lHTjHKTRbxxuLeQwzbzs',
  authDomain: 'polling-app-32e29.firebaseapp.com',
  databaseURL: 'https://polling-app-32e29.firebaseio.com',
  projectId: 'polling-app-32e29',
  storageBucket: 'polling-app-32e29.appspot.com',
  messagingSenderId: '538745060229',
};

class Firebase {
  constructor() {
    firebase.initializeApp(config);
    this.store = firebase.firestore;
    this.auth = firebase.auth;
  }

  get polls() {
    return this.store().collection('polls');
  }
}

export default new Firebase();
